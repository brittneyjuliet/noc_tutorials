let moverA;
let moverB;

function setup() {
	createCanvas(windowWidth, windowHeight);
	// put setup code here

	moverA = new Mover(width/2, height/2);
	moverB = new Mover(width/2 + width/4, height/2);

}

function draw() {
	// put drawing code here
	background(127);

	if (mouseIsPressed){
		let wind = createVector(0.1, 0);
		moverA.applyForce(wind);
		moverB.applyForce(wind);
	}

	let gravity = createVector(0, .25);
	moverA.applyForce(gravity);
	moverB.applyForce(gravity);
	
	moverA.update();
	moverA.edges();
	moverA.show();

	moverB.update();
	moverB.edges();
	moverB.show();
	


}
