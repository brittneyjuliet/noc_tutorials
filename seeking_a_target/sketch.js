let vehicle;
let target;

function setup() {
	createCanvas(windowWidth, windowHeight);
	// put setup code here

	vehicle = new Vehicle(width/2, height/2);

}

function draw() {
	// put drawing code here
	background(127);

	fill(255, 127);
	noStroke();
	target = createVector(mouseX, mouseY);
	circle(target.x, target.y, 16);

	vehicle.seek(target);
	vehicle.update();
	// vehicle.edges();
	vehicle.show();

}
