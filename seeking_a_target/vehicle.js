class Vehicle {

    constructor(x, y){
        this.pos = createVector(x, y);
        this.vel = createVector(0, 0);
        this.acc = createVector(0, 0);
        this.maxSpeed = 4;
        this.maxForce = 0.5;
        this.r = 8;
    
    }

    seek(target){
        let force = p5.Vector.sub(target, this.pos);
        force.setMag(this.maxSpeed);
        force.sub(this.vel);
        force.limit(this.maxForce);
        this.applyForce(force);
    }

    applyForce(force){
        this.acc.add(force);

    }

    edges(){
        if (this.pos.y >= height-this.r){
            this.pos.y = height-this.r;
            this.vel.y *= -1;
        }

        if (this.pos.y <= this.r){
            this.pos.y = this.r;
            this.vel.y *= -1;
        }

        if (this.pos.x >= width - this.r){
            this.pos.x = width-this.r;
            this.vel.x *= -1;
        } else if (this.pos.x <= this.r){
            this.pos.x = this.r;
            this.vel.x *= -1;
        }
        
    }

    update(){

        this.vel.add(this.acc);
        this.vel.limit(this.maxSpeed);
        this.pos.add(this.vel);
        this.acc.set(0, 0);

    }

    show(){
        fill(255);
        noStroke();
        push();
        translate(this.pos.x, this.pos.y);
        rotate(this.vel.heading());
        triangle(-this.r, -this.r/2, -this.r, this.r/2, this.r, 0);
        pop();
    }

}