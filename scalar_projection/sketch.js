let path;

function setup() {
	createCanvas(400, 400);
	// put setup code here

	path = createVector(200, 60);
}

function vectorProjection(a, b){
	let bCopy = b.copy().normalize();
	let sp = a.dot(bCopy);
	bCopy.mult(sp);
	return bCopy;
}

function draw() {
	// put drawing code here
	background(0);
	strokeWeight(4);
	stroke(255);

	let mouse = createVector(mouseX, mouseY);
	let pos = createVector(100, 200);
	let a = p5.Vector.sub(mouse, pos);

	
	line(pos.x, pos.y, pos.x + a.x, pos.y + a.y);
	line(pos.x, pos.y, pos.x + path.x, pos.y + path.y);

	let v = vectorProjection(a, path);
	strokeWeight(8);
	stroke(0, 0, 255);
	line(pos.x, pos.y, pos.x + v.x, pos.y + v.y);

	fill(0,255,0);
	noStroke();
	circle(pos.x, pos.y, 8);

	

}
