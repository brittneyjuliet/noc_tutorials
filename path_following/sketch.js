let path;
let vehicle;
let lineY;
let lineRadius;
let intersection = false;

function setup() {
	createCanvas(400, 400);
	// put setup code here

	// path = createVector(200, 60);
	path = new Path(width/2, 0, width/2, height);
	vehicle = new Vehicle(100, 100);
	// vehicle.vel.x = 2;
	vehicle.vel.y = 1;

	lineY = height/2;
	lineRadius = 2;
	
}

function draw() {
	// put drawing code here
	background(0);

	path.end.x = mouseX;
	
	let force = vehicle.follow(path);
	vehicle.applyForce(force);

	
	vehicle.edges();
	vehicle.update();
	vehicle.show();
	vehicle.checkIntersection();

	path.show();
	
	fill(255, 255);
	strokeWeight(2)
	line(0, lineY, width, lineY);

}
