function findProjection(pos, a, b){
	let v1 = p5.Vector.sub(a, pos);
	let v2 = p5.Vector.sub(b, pos);
	v2.normalize();
	let sp = v1.dot(v2);
	v2.mult(sp);
	v2.add(pos);
	return v2;
}

class Vehicle {

    // let intersection = false;

    constructor(x, y){
        this.pos = createVector(x, y);
        this.vel = createVector(0, 0);
        this.acc = createVector(0, 0);
        this.maxSpeed = 2;
        this.maxForce = 0.5;
        this.r = 8;
    
    }

    seek(target){
        let force = p5.Vector.sub(target, this.pos);
        force.setMag(this.maxSpeed);
        force.sub(this.vel);
        force.limit(this.maxForce);
        this.applyForce(force);
    }

    applyForce(force){
        this.acc.add(force);

    }

    edges(){
        if (this.pos.y >= height-this.r){
            this.pos.y = this.r;
            this.vel.y *= -1;
        } else if (this.pos.y <= this.r){
            this.pos.y = this.r;
            this.vel.y *= -1;
        }

        if (this.pos.x >= width - this.r){
            this.pos.x = this.r;
            this.vel.x *= -1;
        } else if (this.pos.x <= this.r){
            this.pos.x = this.r;
            this.vel.x *= -1;
        }
        
    }

    follow(path){
        // calculate future position
        let future = this.vel.copy();
        future.mult(50);
        future.add(this.pos);
        noStroke();
        fill(255);
        circle(future.x, future.y, 16);

        // is future on path?
        let target = findProjection(path.start, future, path.end);
        fill(0, 255, 0);
        circle(target.x, target.y, 16);

        let d = p5.Vector.dist(future, target);
        if (d > path.radius){
            return this.seek(target);
        } else {
            return createVector(0, 0);
        }
    }
    

    update(){

        this.vel.add(this.acc);
        this.vel.limit(this.maxSpeed);
        this.pos.add(this.vel);
        this.acc.set(0, 0);

    }

    show(){
        fill(255);
        noStroke();
        push();
        translate(this.pos.x, this.pos.y);
        rotate(this.vel.heading());
        triangle(-this.r, -this.r/2, -this.r, this.r/2, this.r, 0);
        pop();
    }

    checkIntersection(){
        if (this.pos.y > lineY && this.pos.y < (lineY + lineRadius)){
            intersection = true;
        } else {
            intersection = false;
        }
        print("intersection: " + intersection);
        // print("posy: " + this.pos.y)
    }

}