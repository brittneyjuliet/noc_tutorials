let mover;

function setup() {
	createCanvas(windowWidth, windowHeight);
	// put setup code here

	mover = new Mover(width/2, height/2);

}

function draw() {
	// put drawing code here
	background(127);

	if (mouseIsPressed){
		let wind = createVector(0.1, 0);
		mover.applyForce(wind);
	}

	let gravity = createVector(0, .25);
	mover.applyForce(gravity);
	
	mover.update();
	mover.edges();
	mover.show();
	


}
