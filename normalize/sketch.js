let mover;

function setup() {
	createCanvas(windowWidth, windowHeight);
	background(127);
	// put setup code here

	mover = new Mover(width/2, height/2);

}

function draw() {
	// put drawing code here
	

	let pos = createVector(width/2, height/2);
	let mouse = createVector(mouseX, mouseY);

	let v = p5.Vector.sub(mouse, pos);
	// let m = v.mag();
	// v.div(m);
	// v.normalize();
	// v.mult(50);
	v.setMag(50);

	translate(width/2, height/2);
	strokeWeight(4);
	stroke(255, 50);
	line(0, 0, v.x, v.y);
	
	// mover.update();
	// mover.show();
	

}
