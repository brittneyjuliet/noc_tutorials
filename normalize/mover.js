class Mover {

    constructor(x, y){
        this.pos = createVector(x, y);
        this.vel = createVector(1,0);
    }

    update(){
        this.pos.add(this.vel);
    }

    show(){
        fill(255, 100);
        noStroke();
        ellipse(this.pos.x, this.pos.y, 25);
    }

}