let particles = [];

function setup() {
	createCanvas(windowWidth, windowHeight);
	// put setup code here

}

function draw() {
	// put drawing code here
	background(127);

	for (let i = 0; i < 5; i++){
		particles.push(new Particle(width/2, height/2));
	}

	// if (mouseIsPressed){
	// 	let wind = createVector(0.1, 0);
	// 	particle.applyForce(wind);
	// }

	for (let particle of particles){
		let gravity = createVector(0, .25);
		particle.applyForce(gravity);
		particle.update();
		// particle.edges();
		particle.show();
	}

	for (let i = particles.length-1; i>=0; i--){
		if (particles[i].finished()){
			particles.splice(i, 1);
		}
	}

}
